from dataclasses import dataclass
import typing
import functools

import utils

@dataclass
class Monkey:
    num: int
    items: typing.List[int]
    worry_op: typing.Tuple[str, str]
    div_by: int
    true_target: int
    false_target: int
    inspect_counter: int = 0

    div_worry_by_3 = True
    worry_mod = None

    def apply_worry_op(self, old):
        (op_fn, op_arg) = self.worry_op

        if op_arg[:3] == "old":
            op_arg = int(old)
        else:
            op_arg = int(op_arg)

        if op_fn == "+":
            return old + op_arg
        elif op_fn == "-":
            return old - op_arg
        elif op_fn == "*":
            return old * op_arg
        else:
            raise ValueError(f"unexpected operator {op_fn}")

    def worry_target(self, item, *args) -> typing.Tuple[int, int]:
        item = item % self.worry_mod

        from_worry_op = self.apply_worry_op(item)
        if self.div_worry_by_3:
            after_bored = from_worry_op // 3
        else:
            after_bored = from_worry_op

        if (after_bored % self.div_by) == 0:
            return (after_bored, self.true_target)
        else:
            return (after_bored, self.false_target)

def parse_monkey(lines):
    num = utils.extract_int(lines[0])
    [_, comma_sep_items] = lines[1].split(": ")
    items = [int(worry) for worry in comma_sep_items.split(",")]

    [_, operation_str] = lines[2].split(" = old ")
    [op_fn, op_arg] = operation_str.split(" ")
    divBy = utils.extract_int(lines[3])
    trueTarget = utils.extract_int(lines[4])
    falseTarget = utils.extract_int(lines[5])

    return Monkey(num, items, (op_fn, op_arg), divBy, trueTarget, falseTarget)

def take_turn(monkeys, n):
    m = monkeys[n]

    for item in m.items:
        m.inspect_counter += 1
        (new_item, target) = m.worry_target(item)
        monkeys[target].items.append(new_item)

    m.items = []

def round(monkeys):
    for idx in range(len(monkeys)):
        take_turn(monkeys, idx)

def get_monkeys(lines):
    monkeys = [parse_monkey(e) for e in utils.divide_chunks(lines, 7)]
    Monkey.worry_mod = functools.reduce(lambda a, b: a * b, (m.div_by for m in monkeys))
    return monkeys

def calc_monkey_business(monkeys):
    top_2_inspections = sorted([m.inspect_counter for m in monkeys])[-2:]
    return top_2_inspections[0] * top_2_inspections[1]

def part1(lines):
    monkeys = get_monkeys(lines)

    for _ in range(20):
        round(monkeys)

    return calc_monkey_business(monkeys)

def part2(lines):
    Monkey.div_worry_by_3 = False
    monkeys = get_monkeys(lines)

    for _ in range(10_000):
        round(monkeys)

    return calc_monkey_business(monkeys)

utils.aocmain(part1, part2, "../inputs/day11.txt")
