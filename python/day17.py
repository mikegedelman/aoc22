from dataclasses import dataclass
from typing import List

import utils


ROCK_SHAPES = [
    [list("####")],
    [list(".#."),
     list("###"),
     list(".#.")],
    [list("..#"),
     list("..#"),
     list("###")],
     [list("#"),
      list("#"),
      list("#"),
      list("#")],
     [list("##"),
      list("##")],
]

example = ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>"
import dataclasses

@dataclasses.dataclass
class Point:
    x: int
    y: int

@dataclasses.dataclass
class Rock:
    shape: List[List[str]]
    position: Point

    def width(self):
        return len(self.shape[0])

    def height(self):
        return len(self.shape)

    def collide(self, map):
        map_lines = self.get_covered_map(map)
        # import ipdb; ipdb.set_trace()

        for map_y in range(len(map_lines)):
            for map_x in range(len(map_lines[map_y])):
                map_point = map_lines[map_y][map_x]
                if map_point != "." and self.shape[map_y][map_x] != ".":
                    return True

        return False

    def get_covered_map(self, map):
        map_lines = map[self.position.y:self.position.y + self.height()]
        for idx in range(len(map_lines)):
            map_lines[idx] = map_lines[idx][self.position.x:self.position.x + self.width()]

        return map_lines

    def try_move(self, map, pos_offset):
        old_pos = copy.deepcopy(self.position)
        self.position.x += pos_offset.x
        self.position.y += pos_offset.y

        if self.collide(map):
            self.position = old_pos
            return False

        return True

def write_rock(map, rock):
    for y_off in range(rock.height()):
        y = rock.position.y + y_off
        for x_off in range(rock.width()):
            x = rock.position.x + x_off
            map[y][x] = rock.shape[y_off][x_off]

    return rock.position.y

import os
import copy
import time
def render(map, rock, tower_y):
    os.system("clear")
    render_map = copy.deepcopy(map)
    write_rock(render_map, rock)

    for ln in render_map: # [0:15]:
        for char in ln:
            print(char, end="", flush=False)
        print("", flush=False)

    print("")
    print("Tower y: ", tower_y)
    time.sleep(0.1)


def part1(jets):
    map = [list("+-------+")]
    blank_tunnel = list("|.......|")
    rock_idx = 0
    jet_idx = 0

    apply_jet = True
    tower_y = 0

    rocks = 0
    for i in range(2022):
        shape = ROCK_SHAPES[rock_idx]
        rock_y = tower_y - len(shape) - 3
        if rock_y < 0:
            map = [copy.deepcopy(blank_tunnel) for _ in range(0 - rock_y)] + map
            rock_y = 0
        rock = Rock(shape, Point(3, rock_y))
        while True:

            if apply_jet:
                apply_jet = False

                direction = jets[jet_idx]
                if direction == ">":
                    rock.try_move(map, Point(1, 0))
                elif direction == "<":
                    rock.try_move(map, Point(-1, 0))

                jet_idx = (jet_idx + 1) % len(jets)
            else:
                apply_jet = True

                moved = rock.try_move(map, Point(0, 1))
                if not moved:
                    tower_y = write_rock(map, rock)
                    rock_idx = (rock_idx + 1) % len(ROCK_SHAPES)
                    apply_jet = True
                    # jet_idx = 0
                    break

            # render(map, rock, tower_y)

        rocks += 1

    print("rocks", rocks)
    print(len(map) - tower_y - 1)

part1(example)
