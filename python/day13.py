from functools import cmp_to_key
import utils

def parse(lines):
    return [eval(l) for l in lines if l.startswith("[")]

def cmp_lists(ll, rl):
    for idx, li in enumerate(ll):
        if idx >= len(rl):
            return -1  # right list ran out of inputs

        ri = rl[idx]
        result = cmp_elements(li, ri)
        if result != 0:
            return result

    # if the lengths are equal, then 0 (which means we have to keep comparing)
    # else 1, which means the left list ran out of inputs
    return 0 if len(ll) == len(rl) else 1

def cmp_elements(le, re):
    if isinstance(le, list) and isinstance(re, list):
        return cmp_lists(le, re)

    if isinstance(le, int) and isinstance(re, int):
        return re - le

    if isinstance(le, list) and isinstance(re, int):
        return cmp_elements(le, [re])

    if isinstance(le, int) and isinstance(re, list):
        return cmp_elements([le], re)

def part1(lines):
    p = parse(lines)
    total = 0
    for idx, pair in enumerate(utils.divide_chunks(p, 2)):
        [l, r] = pair
        result = cmp_lists(l, r)
        if result > 0:
            total += idx + 1

    return total

def part2(lines):
    divider1 = [[2]]
    divider2 = [[6]]
    p = parse(lines) + [divider1, divider2]
    ordered_p = list(reversed(sorted(p, key=cmp_to_key(cmp_lists))))

    div1_index = ordered_p.index([[2]])
    div2_index = ordered_p.index([[6]])

    return (div1_index + 1) * (div2_index + 1)

utils.aocmain(part1, part2, "../inputs/day13.txt")
