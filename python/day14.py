import os
import time
import copy
from dataclasses import dataclass

example = """498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9""".splitlines()

@dataclass
class Point:
    x: int
    y: int

class Cave:
    def __init__(self, lines):
        ((min_x, max_x), (min_y, max_y)) = Cave.get_cave_dimensions(lines)
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y

        self.cur_sand = None
        self.finished = False

        self.build_map(lines)
        self.put(500, 0, "+")

    @staticmethod
    def get_cave_dimensions(lines):
        all_x_coords = [c[0] for line in lines for c in line]
        all_y_coords = [c[1] for line in lines for c in line]

        return (min(all_x_coords), max(all_x_coords)), (0, max(all_y_coords))

    def put(self, x, y, ch):
        self.map[y][x - self.min_x] = ch

    def get(self, point):
        return self.map[point.y][point.x - self.min_x]

    def get_or_none(self, point):
        if point.x < self.min_x or point.x > self.max_x:
            return None
        if point.y < self.min_y or point.y > self.max_y:
            return None

        return self.get(point)

    def done(self):
        """We detected sand hitting the abyss, so stop the simulation."""
        self.finished = True
        self.cur_sand = None

    def is_running(self):
        return not self.finished

    def update(self):
        if self.cur_sand is None:
            self.cur_sand = Point(500, 0)

        if self.cur_sand.y == self.max_y:
            self.done()
            return

        below = self.get_or_none(Point(self.cur_sand.x, self.cur_sand.y + 1))
        below_left = self.get_or_none(Point(self.cur_sand.x - 1, self.cur_sand.y + 1))
        below_right = self.get_or_none(Point(self.cur_sand.x + 1, self.cur_sand.y + 1))

        if below == ".":
            self.cur_sand.y += 1
        elif below is None:
            self.done()
        elif below_left == ".":
            self.cur_sand.y += 1
            self.cur_sand.x -= 1
        elif below_left is None:
            self.done()
        elif below_right == ".":
            self.cur_sand.y += 1
            self.cur_sand.x += 1
        elif below_right is None:
            self.done()
        else: # Sand is now at rest
            self.put(self.cur_sand.x, self.cur_sand.y, "o")
            self.cur_sand = None

    def render(self):
        render_map = copy.deepcopy(self.map)
        if self.cur_sand:
            render_map[self.cur_sand.y][self.cur_sand.x - self.min_x] = "o"

        start_x_label = [c for c in str(self.min_x)]
        _500_label = ["5", "0", "0"]
        end_x_label = [c for c in str(self.max_x)]

        header = []
        for i in range(3):
            hs = " \t" + start_x_label[i] + (" " * (500 - self.min_x - 1)) + _500_label[i] + (" " * (self.max_x - 500 - 1)) + end_x_label[i]
            header.append(hs)

        for hln in header:
            print(hln)

        for idx, ln in enumerate(render_map):
            print(f"{idx}\t", end="")
            for ch in ln:
                print(ch, flush=False, end="")
            print(flush=False)


    def save_line_segment(self, begin, end):
        (begin_x, begin_y) = begin
        (end_x, end_y) = end

        if begin_x == end_x: # line is vertical
            start = min(begin_y, end_y)
            stop = max(begin_y, end_y)

            for y in range(start, stop + 1):
                self.put(begin_x, y, "#")

        elif begin_y == end_y: # line is horizontal
            start = min(begin_x, end_x)
            stop = max(begin_x, end_x)

            for x in range(start, stop + 1):
                self.put(x, begin_y, "#")

        else: # diagonal: unsupported
            raise ValueError("found a diagnonal line")

    def save_line(self, line_in):
        line = copy.deepcopy(line_in)

        while len(line) >= 2:
            [begin, end] = line[:2]
            self.save_line_segment(begin, end)
            line.pop(0)

    def build_map(self, lines):
        self.map = [["."] * ((self.max_x + 1) - self.min_x) for _ in range(self.min_y, self.max_y + 1)]

        for line in lines:
            self.save_line(line)

    def count_sand(self):
        count = 0
        for line in self.map:
            for x in line:
                if x == "o":
                    count += 1

        return count

class CavePart2(Cave):
    def __init__(self, lines, min_x, max_x):
        (_, (min_y, max_y)) = Cave.get_cave_dimensions(lines)
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y + 2

        self.cur_sand = None
        self.finished = False

        self.build_map(lines)
        self.put(500, 0, "+")

    def get_or_none(self, point):
        if point.x < self.min_x or point.x > self.max_x:
            raise ValueError("need a bigger map")
        if point.y < self.min_y or point.y > self.max_y:
            raise ValueError("need a bigger map")

        return self.get(point)

    def update(self):
        if self.cur_sand is None:
            self.cur_sand = Point(500, 0)

        if self.cur_sand.y == self.max_y:
            raise ValueError("sand fell into the abyss, which isn't possible anymore")

        below = self.get_or_none(Point(self.cur_sand.x, self.cur_sand.y + 1))
        below_left = self.get_or_none(Point(self.cur_sand.x - 1, self.cur_sand.y + 1))
        below_right = self.get_or_none(Point(self.cur_sand.x + 1, self.cur_sand.y + 1))

        if below == ".":
            self.cur_sand.y += 1

        elif below_left == ".":
            self.cur_sand.y += 1
            self.cur_sand.x -= 1

        elif below_right == ".":
            self.cur_sand.y += 1
            self.cur_sand.x += 1

        else: # Sand is now at rest
            self.put(self.cur_sand.x, self.cur_sand.y, "o")
            if self.cur_sand.x == 500 and self.cur_sand.y == 0:
                self.done()
            self.cur_sand = None

    def build_map(self, lines):
        self.map = [["."] * ((self.max_x + 1) - self.min_x) for _ in range(self.min_y, self.max_y + 1)]
        for x in range(len(self.map[0])):
            self.map[-1][x] = "#"

        for line in lines:
            self.save_line(line)

def parse(lines):
    parsed = []
    for line in lines:
        coords = line.split(" -> ")
        split_coords = [c.split(",") for c in coords]
        parsed.append([(int(sc[0]), int(sc[1])) for sc in split_coords])
    return parsed

def simulate_cave(cave, draw=False):
    while cave.is_running():
        cave.update()
        if draw:
            os.system("clear")
            cave.render()
            time.sleep(0.01)

    return cave.count_sand()

def part1(lines):
    parsed = parse(lines)
    cave = Cave(parsed)
    sand_at_rest = simulate_cave(cave)
    print(f"Part 1 finished! Sand at rest: {sand_at_rest}")

def part2(lines):
    draw_example = False  # switch this to True to animate the example for part2

    if draw_example:
        parsed = parse(example)
        cave = CavePart2(parsed, 480, 520)
        sand_at_rest = simulate_cave(cave, draw=True)
    else:
        parsed = parse(lines)
        cave = CavePart2(parsed, 0, 1000)
        sand_at_rest = simulate_cave(cave)
    print(f"Part 2 finished! Sand at rest: {sand_at_rest}")


def main():
    with open("../inputs/day14.txt") as f:
        lines = f.readlines()

    part1(lines)
    part2(lines)

main()
