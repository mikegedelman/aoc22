import pprint

pp = pprint.PrettyPrinter()

def divide_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def extract_int(s):
    return int(''.join(c for c in s if c.isdigit()))

def aocmain(part1, part2, filename):
    with open(filename) as f:
        lines = f.readlines()

    print("Part 1:", part1(lines))
    print("Part 2:", part2(lines))

def transpose(l):
    return list(map(list, zip(*l)))

def prettyprint(s):
    pp.pprint(s)
