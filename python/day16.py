from dataclasses import dataclass
from typing import List, Dict

import utils

@dataclass
class Valve:
    name: str
    flow_rate: int

    connections: List["Valve"] = None

    def __hash__(self):
        return hash(self.name)

    def __repr__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

def parse(lines) -> Dict[str, Valve]:
    valve_connections = {}
    valves = {}
    for line in lines:
        [valve_str, r] = line.strip().split(" has flow rate=")
        valve_name = valve_str[-2:]
        flow_rate = utils.extract_int(r)
        valves[valve_name] = Valve(valve_name, flow_rate)

        children_list = "".join(c for c in r.split("valve")[1] if c not in "s ")
        children_strs = children_list.split(",")
        valve_connections[valve_name] = children_strs

    # print(valves)
    for valve_name, children in valve_connections.items():
        valve = valves[valve_name]
        valve.connections = [valves[child_name] for child_name in children]

    return valves

def part1(lines):
    valves = parse(lines)
    start = valves["AA"]

    num_useful_valves = len([v for v in valves.values() if v.flow_rate > 0])

    prev_states = {}
    prev_states[(start, frozenset())] = 0

    def save(s, key, new_pressure):
        existing = s.get(key, None)
        if not existing or new_pressure > existing:
            s[key] = new_pressure

    for time_left in reversed(range(30)):
        cur_states = {}

        for (cur_valve, valves_open), pressure in prev_states.items():
            if time_left < 26 and pressure == 0:
                continue

            new_pressure = pressure + sum([v.flow_rate for v in valves_open])

            if len(valves_open) == num_useful_valves:
                save(cur_states, (cur_valve, valves_open), new_pressure)

            if cur_valve not in valves_open and cur_valve.flow_rate > 0:
                save(cur_states, (cur_valve, valves_open | frozenset([cur_valve])), new_pressure)

            for nearby_valve in cur_valve.connections:
                save(cur_states, (nearby_valve, valves_open), new_pressure)

        prev_states = cur_states

    best = max(cur_states, key=cur_states.get)
    return cur_states[best]


def part2(lines):
    valves = parse(lines)
    start = valves["AA"]

    num_useful_valves = len([v for v in valves.values() if v.flow_rate > 0])

    prev_states = {}
    prev_states[(start, start, frozenset())] = 0

    def save(s, key, new_pressure):
        existing = s.get(key, None)
        if not existing or new_pressure > existing:
            s[key] = new_pressure

    for time_left in reversed(range(26)):
        print("Time left:", time_left)
        cur_states = {}

        for (cur_valve, elephant_valve, valves_open), pressure in prev_states.items():
            if time_left < 22 and pressure == 0:
                continue

            if time_left < 15 and pressure < 100:
                continue

            if time_left < 12 and pressure < 250:
                continue

            if time_left < 10 and pressure < 500:
                continue

            new_pressure = pressure + sum([v.flow_rate for v in valves_open])

            if len(valves_open) == num_useful_valves:
                save(cur_states, (cur_valve, elephant_valve, valves_open), new_pressure)

            i_can_open = False
            if cur_valve not in valves_open and cur_valve.flow_rate > 0:
                i_can_open = True
                save(cur_states, (cur_valve, elephant_valve, valves_open | frozenset([cur_valve])), new_pressure)

            elephant_can_open = False
            if elephant_valve not in valves_open and elephant_valve.flow_rate > 0:
                elephant_can_open = True
                save(cur_states, (cur_valve, elephant_valve, valves_open | frozenset([elephant_valve])), new_pressure)

            if i_can_open and elephant_can_open:
                save(cur_states, (cur_valve, elephant_valve, valves_open | frozenset([cur_valve, elephant_valve])), new_pressure)

            for nearby_me_valve in cur_valve.connections:
                    for nearby_elephant_valve in elephant_valve.connections:
                        if nearby_me_valve in (nearby_elephant_valve, elephant_valve) or nearby_elephant_valve in (nearby_me_valve, cur_valve):
                            continue

                        save(cur_states, (nearby_me_valve, nearby_elephant_valve, valves_open), new_pressure)

                        if i_can_open:
                            save(cur_states, (cur_valve, nearby_elephant_valve, valves_open | frozenset([cur_valve])), new_pressure)
                        if elephant_can_open:
                            save(cur_states, (nearby_me_valve, elephant_valve, valves_open | frozenset([elephant_valve])), new_pressure)

        prev_states = cur_states

    best = max(cur_states, key=cur_states.get)
    return cur_states[best]


utils.aocmain(part1, part2, "../inputs/day16.txt")
