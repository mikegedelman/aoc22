import math
import queue
import utils


def pos_height(val):
    if val == "S":
        return ord("a")
    if val == "E":
        return ord("z")
    return ord(val)

def step_height(from_, to):
    return pos_height(to) - pos_height(from_)

def get_surrounding(lines, x, y):
    ret = []
    if x > 0:
        ret.append((x - 1, y, lines[y][x - 1]))
    if x < (len(lines[0]) - 1):
        ret.append((x + 1, y, lines[y][x + 1]))
    if y > 0:
        ret.append((x, y - 1, lines[y - 1][x]))
    if y < (len(lines) - 1):
        ret.append((x, y + 1, lines[y + 1][x]))

    return ret

def get_valid_steps(lines, x, y):
    surrounding = get_surrounding(lines, x, y)
    cur = lines[y][x]
    return [(s[0], s[1]) for s in surrounding if step_height(s[2], cur) <= 1]

def find_in(lines, target):
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x] == target:
                return (x, y)

def dijkstra(lines):
    start = find_in(lines, "E")
    visited = set()

    weights = [[math.inf] * len(lines[0]) for _ in range(len(lines))]
    startx, starty = start
    weights[starty][startx] = 0

    q = queue.PriorityQueue()
    q.put((0, start))

    while not q.empty():
        (cur_weight, cur) = q.get()
        if cur in visited:
            continue

        cur_x, cur_y = cur
        neighbors = get_valid_steps(lines, cur_x, cur_y)
        unvis_neighbors = set(neighbors) - visited

        for n in unvis_neighbors:
            nx, ny = n
            n_weight = weights[ny][nx]
            new_n_weight = cur_weight + 1
            if new_n_weight < n_weight:
                weights[ny][nx] = new_n_weight

            q.put((weights[ny][nx], n))

        visited.add((cur_x, cur_y))

    return weights

def part1(lines):
    weights = dijkstra(lines)

    end = find_in(lines, "S")
    endx, endy = end
    return weights[endy][endx]

def part2(lines):
    weights = dijkstra(lines)

    coords_at_height_a = []
    for y in range(len(lines)):
        for x in range(len(lines[0])):
            if lines[y][x] == "a":
                coords_at_height_a.append((x, y))

    elev_a_weights = [weights[y][x] for x, y in coords_at_height_a]

    return min(elev_a_weights)

utils.aocmain(part1, part2, "../inputs/day12.txt")
