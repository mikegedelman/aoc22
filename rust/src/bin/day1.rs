extern crate aoc22;

use std::io::BufRead;
use aoc22::{get_input_bufreader};

fn get_elves_calories(values: Vec<Option<i32>>) -> Vec<i32> {
    let mut elves_calories: Vec<i32>  = vec![];
    let mut current_running_total = 0;

    for value in values {
        match value {
            Some(cals) => {
                current_running_total += cals;
            },
            None => {
                elves_calories.push(current_running_total);
                current_running_total = 0;
            },
        };
    }

    // Save the current remaining total since there's no blank line at the end of the file.
    elves_calories.push(current_running_total);

    elves_calories
}

fn part1(values: Vec<Option<i32>>) -> i32 {
    let elves_calories = get_elves_calories(values);
    *elves_calories.iter().max().unwrap()
}

fn part2(values: Vec<Option<i32>>) -> i32 {
    let mut elves_calories = get_elves_calories(values);
    elves_calories.sort();
    elves_calories.reverse();
    elves_calories[0..3].iter().sum()
}

fn bufread_to_values<A: BufRead>(buf: A) -> Vec<Option<i32>> {
    buf.lines()
    .into_iter()
    .map(|r| {
        let s = r.unwrap();
        if s == "" {
            None
        } else {
            Some(s.parse::<i32>().unwrap())
        }
    })
    .collect()
}

fn main() {
    let buf = get_input_bufreader();
    let values: Vec<Option<i32>> = bufread_to_values(buf);
    let part1_result = part1(values.clone());
    println!("Part 1: The elf carrying the most calories has: {}", part1_result);

    let part2_result = part2(values);
    println!("Part 1: The sum of the top three elves' calories is: {}", part2_result);
}

#[cfg(test)]
mod tests {
    use std::io::BufReader;

    use crate::*;

    static EXAMPLE_INPUT: &str = r#"1000
2000
3000

4000

5000
6000

7000
8000
9000

10000"#;

    #[test]
    fn part1_example() {
        let buf = BufReader::new(EXAMPLE_INPUT.as_bytes());
        let values = bufread_to_values(buf);
        let answer = part1(values);
        assert_eq!(answer, 24000);
    }

    #[test]
    fn part2_example() {
        let buf = BufReader::new(EXAMPLE_INPUT.as_bytes());
        let values = bufread_to_values(buf);
        let answer = part2(values);
        assert_eq!(answer, 45000);
    }
}
