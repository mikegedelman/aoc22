use std::{io, env, fs::File};

fn get_filename_from_args() -> String {
    let args: Vec<String> = env::args().collect();
    match args.len() {
        2 => args[1].clone(),
        _ => {
            eprintln!("Usage: {} [filename]", args[0]);
            std::process::exit(1);
        }
    }
}

fn bufreader_from_filename(filename: &str) -> io::BufReader<File> {
    let f = File::open(filename).expect("Unable to open the specified file.");
    io::BufReader::new(f)
}

pub fn get_input_bufreader() -> io::BufReader<File> {
    let filename = get_filename_from_args();
    bufreader_from_filename(&filename)
}
