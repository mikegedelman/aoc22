/* Compile:
 *
 *    nvcc day15.cu -o target/day15
 */
#include <iostream>
#include <vector>
#include <math.h>
#include <cstdint>
#include <algorithm>

class Point {
public:
    int x;
    int y;
};

__device__ __host__
inline int dist(Point &a, Point &b) {
    return abs(a.x - b.x) + abs(a.y - b.y);
}

__host__
class Sensor {
public:
    int radius;
    Point location;
    Point closest_beacon;

    Sensor(Point l, Point b) : location(l), closest_beacon(b) {
        radius = dist(location, closest_beacon);
    }
};

#define ROWSIZE 4000000

__global__
void check(uint64_t total, Sensor *sensors, int num_sensors)
{
  unsigned long long int index = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned long long int stride = blockDim.x * gridDim.x;

  for (unsigned long long int i = index; i < total; i += stride) {
    unsigned long long int y = i / ROWSIZE;
    unsigned long long int x = i % ROWSIZE;

    bool isCovered = false;
    for (int j = 0; j < num_sensors; j++) {
      Sensor s = sensors[j];

      if (s.closest_beacon.x == x && s.closest_beacon.y == y) {
        isCovered = true;
      }

      Point p {x, y};
      int distance = dist(s.location, p);
      if (distance <= s.radius) {
        isCovered = true;
      }
    }

    if (!isCovered) {
      printf("Found it! %llu, %llu\n", x, y);
      printf("Answer: %llu\n", (x * 4000000) + y);
      break;
    }
  }
}

__host__
bool point_vec_contains(std::vector<Point> &ps, Point &p1) {
    auto find_result = std::find_if(std::begin(ps), std::end(ps), [p1](Point p2){ return p1.x == p2.x && p1.y == p2.y; });
    return find_result != std::end(ps);
}

__host__
int part1(std::vector<Sensor> &sensors, int search_y) {
    std::vector<Point> beacons;
    std::vector<Point> beacons_and_sensors;
    int max_radius = 0;
    for (auto s : sensors) {
        beacons.push_back(s.closest_beacon);
        beacons_and_sensors.push_back(s.location);
        beacons_and_sensors.push_back(s.closest_beacon);
        max_radius = std::max(s.radius, max_radius);
    }

    int min_x = 0, max_x = 0;
    for (auto p : beacons_and_sensors) {
        min_x = std::min(p.x, min_x);
        max_x = std::max(p.x, max_x);
    }

    int covered_count = 0;
    for (int x = min_x - max_radius - 100; x < max_x + max_radius + 100; x++) {
        Point p1 {x, search_y};

        if (point_vec_contains(beacons, p1)) {
            continue;
        }

        for (auto sensor : sensors) {
            auto distance = dist(sensor.location, p1);
            if (distance <= sensor.radius) {
                covered_count++;
                break;
            }
        }
    }

    return covered_count;
}

int main(void)
{
  // I didn't feel like parsing the input in C++...
  std::vector<Sensor> example {
    {{2, 18}, {-2, 15}},
    {{9, 16}, {10, 16}},
    {{13, 2}, {15, 3}},
    {{12, 14},  {10, 16}},
    {{10, 20},  {10, 16}},
    {{14, 17},  {10, 16}},
    {{8, 7}, {2, 10}},
    {{2, 0}, {2, 10}},
    {{0, 11},  {2, 10}},
    {{20, 14},  {25, 17}},
    {{17, 20},  {21, 22}},
    {{16, 7}, {15, 3}},
    {{14, 3}, {15, 3}},
    {{20, 1}, {15, 3}}
  };

  std::vector<Sensor> input {
      {{3859432, 2304903}, {3677247, 3140958}},
      {{2488890, 2695345}, {1934788, 2667279}},
      {{3901948, 701878}, {4095477, 368031}},
      {{2422190, 1775708}, {1765036, 2000000}},
      {{2703846, 3282799}, {2121069, 3230302}},
      {{172003, 2579074}, {-77667, 3197309}},
      {{1813149, 1311283}, {1765036, 2000000}},
      {{1704453, 2468117}, {1934788, 2667279}},
      {{1927725, 2976002}, {1934788, 2667279}},
      {{3176646, 1254463}, {2946873, 2167634}},
      {{2149510, 3722117}, {2121069, 3230302}},
      {{3804434, 251015}, {4095477, 368031}},
      {{2613561, 3932220}, {2121069, 3230302}},
      {{3997794, 3291220}, {3677247, 3140958}},
      {{98328, 3675176}, {-77667, 3197309}},
      {{2006541, 2259601}, {1934788, 2667279}},
      {{663904, 122919}, {1618552, -433244}},
      {{1116472, 3349728}, {2121069, 3230302}},
      {{2810797, 2300748}, {2946873, 2167634}},
      {{1760767, 2024355}, {1765036, 2000000}},
      {{3098487, 2529092}, {2946873, 2167634}},
      {{1716839, 634872}, {1618552, -433244}},
      {{9323, 979154}, {-245599, 778791}},
      {{1737623, 2032367}, {1765036, 2000000}},
      {{26695, 3049071}, {-77667, 3197309}},
      {{3691492, 3766350}, {3677247, 3140958}},
      {{730556, 1657010}, {1765036, 2000000}},
      {{506169, 3958647}, {-77667, 3197309}},
      {{2728744, 23398}, {1618552, -433244}},
      {{3215227, 3077078}, {3677247, 3140958}},
      {{2209379, 3030851}, {2121069, 3230302}}
  };
  auto sensorsVec = input;

  std::cout << "Part 1: " << part1(sensorsVec, 2000000) << std::endl;

  Sensor *sensors;
  cudaMallocManaged(&sensors, sizeof(Sensor) * sensorsVec.size());

  int i = 0;
  for (auto s : sensorsVec) {
    sensors[i] = sensorsVec[i];
    i++;
  }

  int num_sensors = sensorsVec.size();
  uint64_t total = (uint64_t)ROWSIZE * (uint64_t)ROWSIZE;

  std::cout << "Part 2: Computing on GPU. This may take some time..." << std::endl;
  check<<<65535, 1024>>>(total, sensors, num_sensors);

  int err = cudaPeekAtLastError();
  if (err) {
    std::cout << "CUDA error: " << err << std::endl;
  }

  cudaDeviceSynchronize();
  cudaFree(sensors);

  return 0;
}
