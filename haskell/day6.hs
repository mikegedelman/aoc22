import System.Environment
import Data.List

example = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"

-- nub: remove duplicate elements from a list
charsAreUnique :: String -> Bool
charsAreUnique s = (length $ nub s) == length s

findUnique :: String -> Int -> Int
findUnique s n = go s n 0
    where
        go [] n 0 = error $ "didn't find a unique string of " ++ (show n)
        go s n count
            | charsAreUnique (take n s) = count + n
            | otherwise = go (drop 1 s) n (count + 1)

-- Main
main' filename = do
   input <- readFile filename

   putStrLn $ "Part 1: " ++ show (findUnique input 4)
   putStrLn $ "Part 2: " ++ show (findUnique input 14)

main = do
   args <- getArgs
   main' (args !! 0)