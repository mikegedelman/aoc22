import Data.Char
import Data.Foldable
import Data.List
import Data.Maybe
import qualified Data.Sequence as Seq
import System.Environment
import Text.Regex.TDFA
import Text.Regex.TDFA.Text ()

example = [ "    [D]    ", "[N] [C]    ", "[Z] [M] [P]", " 1   2   3 ", "", "move 1 from 2 to 1", "move 3 from 1 to 3", "move 2 from 2 to 1", "move 1 from 1 to 2" ]

-- Crate parsing
-- crate indexes are at offsets: 1, 5, 9, 13
-- create a (lazy) list of offsets
getCrateIndexOffset n = 1 + ((n - 1)* 4)
crateIndexOffsets = [getCrateIndexOffset n | n <- [1..]]

-- figure out how many offsets we'll need for the input string length
-- then get the strings at the given offsets
getCratesInLine l =
    let crateIndices = takeWhile (\x -> x < length l) crateIndexOffsets
      in [ l !! i | i <- crateIndices ]

-- "[Z] [M] [P]" -> "ZMP"
-- then reverse and transpose to get one list per crate stack
-- finally, filter out spaces " " and convert to a Seq for easier updates
getCrateLists rawCrateLines =
    let cratesRaw = map getCratesInLine rawCrateLines
        cratesRaw' = reverse cratesRaw
        crates = transpose cratesRaw'
        crates' = map (filter (\x -> x /= ' ')) crates
    in Seq.fromList $ map reverse crates'

crateAt crateLists n = fromJust $ crateLists Seq.!? n

-- Move list parsing: regex, pull out 3 numbers from each string
getMovesInLine :: String -> [String]
getMovesInLine l = getAllTextMatches( l =~ "[0-9]+" ) :: [String]

moveStrToInts s = map (\x -> read x ::Int) s

getMoveList ls =
    let moveLineStrs = map getMovesInLine $ ls
        moveLinesUnadjusted = map moveStrToInts moveLineStrs
    in map adjustMoveLine moveLinesUnadjusted

-- Subtract 1 from the src and destination indexes, for 0-indexing
-- Convert to a tuple for easier destructuring
adjustMoveLine l =
    let (num:rest) = l
        rest' = map (subtract 1) rest
        l' = num:rest'
    in to3Tuple l'

to3Tuple [a, b, c] = (a, b, c)
to3Tuple x = error $ "expected a list of 3, got" ++ (show x)

-- Apply one move: build new lists for src and dest and then swap them in
applyOneMove crateLists src dest =
    let (targetCrate:newSrc) = crateAt crateLists src
        newDest = targetCrate : (crateAt crateLists dest)
        crateLists' = Seq.update src newSrc crateLists
    in Seq.update dest newDest crateLists'

-- Apply num moves in a row
applyMoves crateLists 0 src dest = crateLists
applyMoves crateLists num src dest =
    let crateLists' = applyOneMove crateLists src dest
    in applyMoves crateLists' (num - 1) src dest

-- Apply the move list to the crates, and return the new state of the crates
applyMoveList crateLists [] = crateLists
applyMoveList crateLists moveList =
    let (nextMove:moveList') = moveList
        (num, src, dest) = nextMove
        crateLists' = applyMoves crateLists num src dest
    in applyMoveList crateLists' moveList'

-- Part 2
-- Move num crates from src to dest, retaining their order
applyMovePart2 crateLists num src dest =
    let (targetCrates, newSrc) = splitAt num $ crateAt crateLists src
        newDest = targetCrates ++ (crateAt crateLists dest)
        crateLists' = Seq.update src newSrc crateLists
    in Seq.update dest newDest crateLists'

-- Apply the part 2 move strategy to the list of crates, returning the new state
applyMoveListPart2 crateLists [] = crateLists
applyMoveListPart2 crateLists moveList =
    let (nextMove:moveList') = moveList
        (num, src, dest) = nextMove
        crateLists' = applyMovePart2 crateLists num src dest
    in applyMoveListPart2 crateLists' moveList'

-- Detect the end of the crate list, for splitting the different sections of the input
isEndOfCrates l = (take 2 l) == " 1"

-- Do all parsing, returning inital state for the crates, and a list of moves
getCratesAndMoveList ls =
    let (crateLines, rest) = span (not . isEndOfCrates) ls
        crateLists = getCrateLists crateLines
        moveList = getMoveList $ drop 2 rest
    in (crateLists, moveList)

part1 ls =
    let (crateLists, moveList) = getCratesAndMoveList ls
        finalCrateLists = applyMoveList crateLists moveList
    in toList $ fmap head finalCrateLists

part2 ls =
    let (crateLists, moveList) = getCratesAndMoveList ls
        finalCrateLists = applyMoveListPart2 crateLists moveList
    in toList $ fmap head finalCrateLists

-- Main
getFileLines filename = do
    content <- readFile filename
    return $ lines content

main' filename = do
   linesOfFile <- getFileLines filename

   print $ "Part 1: " ++ show (part1 linesOfFile)
   print $ "Part 2: " ++ show (part2 linesOfFile)

main = do
   args <- getArgs
   main' (args !! 0)
