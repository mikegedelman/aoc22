import System.Environment
import Data.Char
import qualified Data.Set as Set

example = [ "vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "PmmdzqPrVvPwwTWBwg", "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "ttgJtRGJQctTZtZT", "CrZsJsPPZsGzwwsLwLmpwMDw" ]

splitInHalf :: [a] -> ([a], [a])
splitInHalf l = splitAt ((length l + 1) `div` 2) l

charPriority :: Char -> Int
charPriority c
    | c `elem` ['a' .. 'z'] = ((ord c) - (ord 'a')) + 1
    | c `elem` ['A' .. 'Z'] = ((ord c) - (ord 'A')) + 1 + 26
    | otherwise = error $ "encountered invalid char: " ++ [c]

-- for unpacking an element from a set
-- idk why this works lol
-- https://stackoverflow.com/questions/55953490/finding-head-of-a-list-using-foldl-and-the-last-element-using-foldr
setHead :: Set.Set b -> b
setHead = foldr (\x _ -> x) undefined

checkCompartments :: String -> String -> Char
checkCompartments a b =
    let aSet = Set.fromList a
        bSet = Set.fromList b
        common = aSet `Set.intersection` bSet
    in setHead common

priorityForCompartment :: String -> Int
priorityForCompartment compartment =
    let (a, b) = splitInHalf compartment
        common = checkCompartments a b
    in charPriority common

part1 l = sum $ map priorityForCompartment l

-- Part 2 stuff

groupByN :: [a] -> Int -> [[a]]
groupByN [] n = []
groupByN xs n = (take n $ xs):(groupByN (drop n $ xs) n)

searchGroup :: [String] -> Char
searchGroup group =
    let sets = map Set.fromList group
        common = foldr1 Set.intersection sets
    in setHead common

priorityForGroup :: [String] -> Int
priorityForGroup group =
    let common = searchGroup group
    in charPriority common

part2 l =
    let groups = groupByN l 3
    in sum $ map priorityForGroup groups

-- Main
getFileLines filename = do
    content <- readFile filename
    return $ lines content

main' filename = do
   linesOfFile <- getFileLines filename

   print $ "Part 1: " ++ show (part1 linesOfFile)
   print $ "Part 2: " ++ show (part2 linesOfFile)

main = do
   args <- getArgs
   main' (args !! 0)
