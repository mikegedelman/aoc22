import Data.List.Split

example = [ "Monkey 0:", "  Starting items: 79, 98", "  Operation: new = old * 19", "  Test: divisible by 23", "    If true: throw to monkey 2", "    If false: throw to monkey 3", "", "Monkey 1:", "  Starting items: 54, 65, 75, 74", "  Operation: new = old + 6", "  Test: divisible by 19", "    If true: throw to monkey 2", "    If false: throw to monkey 0", "", "Monkey 2:", "  Starting items: 79, 60, 97", "  Operation: new = old * old", "  Test: divisible by 13", "    If true: throw to monkey 1", "    If false: throw to monkey 3", "", "Monkey 3:", "  Starting items: 74", "  Operation: new = old + 3", "  Test: divisible by 17", "    If true: throw to monkey 0", "    If false: throw to monkey 1" ]

data Monkey = Monkey { items :: [String]
                     , operation :: Int -> Int
                     , testDivBy :: Int
                     , trueTarget :: Int
                     , falseTarget :: Int
                     }

parseOperation :: String -> (Int -> Int -> Int)
parseOperation "+" = (+)
parseOperation "-" = flip (-)
parseOperation "*" = (*)
-- parseOperation "/" = flip (/)

mkOperation :: String -> String -> (Int -> Int)
mkOperation opFn "old" = \old -> (parseOperation opFn) old old
mkOperation opFn opArg = (parseOperation opFn) (read opArg :: Int)

parseMonkey :: [[String]] -> Monkey
parseMonkey (_:lines) =
    let itemStrs = drop 2 $ head lines
        monkeyItems =  (map (filter (\x -> x /= ',')) itemStrs)
        (operationFn:operationArg:_) = drop 4 $ lines !! 1
        op = mkOperation operationFn operationArg
        (divBy:_) = drop 3 $ lines !! 2
        (trueTarget:_) = drop 5 $ lines !! 3
        (falseTarget:_) = drop 5 $ lines !! 4
    in Monkey monkeyItems op (read divBy :: Int) (read trueTarget :: Int) (read falseTarget :: Int)

part1 = map (parseMonkey . map words) . chunksOf 7

-- part1 = map (parseMonkey . words)

-- parseLine ("Starting":"items:":rest) =
