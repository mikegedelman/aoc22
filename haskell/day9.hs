import Control.Monad.Trans.State (State, evalState, get, put)
import Control.Monad (replicateM)
import qualified Data.Set as Set
import Utils

type Coord = (Int, Int)

newHeadPos (x, y) "U" = (x, y + 1)
newHeadPos (x, y) "D" = (x, y - 1)
newHeadPos (x, y) "L" = (x - 1, y)
newHeadPos (x, y) "R" = (x + 1, y)

followHead (headX, headY) (tailX, tailY)
    | distance < 1.5 = tailCoords -- do nothing - tail is touching (~1.41 is the distance for diagonal)
    | headX == tailX = if tailY < headY then (headX, headY - 1) else (headX, headY + 1) -- same X == aligned vertically
    | headY == tailY = if tailX < headX then (headX - 1, headY) else (headX + 1, headY) -- same Y == aligned horizontally
    | diff == ( 2,  2) = (headX - 1, headY - 1) -- head is two spaces away diagonally, upper right
    | diff == ( 2, -2) = (headX - 1, headY + 1) -- head is lower right
    | diff == (-2,  2) = (headX + 1, headY - 1) -- head is upper left
    | diff == (-2, -2) = (headX + 1, headY + 1) -- head is lower left
    | (headX - tailX) == 2 = (headX - 1, headY) -- tail is two spaces to the left and either up or down
    | (tailX - headX) == 2 = (headX + 1, headY) -- tail is two spaces to the right and "
    | (headY - tailY) == 2 = (headX, headY - 1) -- tail is two spaces below and either to the left or right
    | (tailY - headY) == 2 = (headX, headY + 1) -- tail is two spaces above and "
    | otherwise = error "unable to determine next position in followHead"
    where tailCoords = (tailX, tailY)
          distance = sqrt $ fromIntegral (((headX - tailX) ^ 2) + ((headY - tailY) ^ 2))
          diff = (headX - tailX, headY - tailY)

updateKnots (h:rest) = updateKnots' rest [h]
updateKnots' [] accum = reverse accum
updateKnots' (t:rest) (h:accum') = updateKnots' rest ((followHead h t):h:accum')

stepPosition :: String -> State [Coord] Coord
stepPosition direction = do
    knots <- get
    let (oldHead:theTails) = knots
    let newHeadCoord = newHeadPos oldHead direction
    let newKnots = updateKnots (newHeadCoord:theTails)
    put newKnots
    return $ last newKnots

updatePosition (direction:distance:_) = replicateM (read distance :: Int) $ stepPosition direction

runWithKnots n = length . Set.fromList . concat . flip evalState (replicate n (0, 0)) . mapM (updatePosition . words)

part1 = runWithKnots 2
part2 = runWithKnots 10
main = aocMain part1 part2 "../inputs/day9.txt"
