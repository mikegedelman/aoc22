import System.Environment
import Data.List.Split

example = [ "A X", "C Y", "B Z" ]

data RPS
    = Rock
    | Paper
    | Scissors

parsePlay :: String -> RPS
parsePlay "A" = Rock
parsePlay "B" = Paper
parsePlay "C" = Scissors
parsePlay "X" = Rock
parsePlay "Y" = Paper
parsePlay "Z" = Scissors
parseOpp x = error $ "invalid input: " ++ x

outcomeScore :: RPS -> RPS -> Int
outcomeScore Rock Paper = 6
outcomeScore Rock Rock = 3
outcomeScore Rock Scissors = 0
outcomeScore Paper Scissors = 6
outcomeScore Paper Paper = 3
outcomeScore Paper Rock = 0
outcomeScore Scissors Rock = 6
outcomeScore Scissors Scissors = 3
outcomeScore Scissors Paper = 0

myScore :: RPS -> Int
myScore Rock = 1
myScore Paper = 2
myScore Scissors = 3

win :: RPS -> RPS
win Rock = Paper
win Paper = Scissors
win Scissors = Rock

draw :: RPS -> RPS
draw x = x -- identity

lose :: RPS -> RPS
lose Rock = Scissors
lose Paper = Rock
lose Scissors = Paper

strategyFromString :: String -> (RPS -> RPS)
strategyFromString "X" = lose
strategyFromString "Y" = draw
strategyFromString "Z" = win
strategyFromString x = error $ "Invalid player move: " ++ x

score :: RPS -> RPS -> Int
score opp me = (outcomeScore opp me) + (myScore me)

scoreLine :: (RPS, RPS) -> Int
scoreLine t =
    let (opp, me) = t
    in score opp me

splitIn2 :: String -> (String, String)
splitIn2 s =
    let splitLn = splitOn " " s
        (a:xs) = splitLn
        (b:_) = xs
    in (a, b)

getPlaysPart1 :: String -> (RPS, RPS)
getPlaysPart1 ln = 
    let (opp, me) = splitIn2 ln
    in (parsePlay opp, parsePlay me)

getPlaysPart2 :: String -> (RPS, RPS)
getPlaysPart2 ln =
    let (opp, me) = splitIn2 ln
        oppPlay = parsePlay opp
        myStrategy = strategyFromString me
        myPlay = myStrategy oppPlay
     in (oppPlay, myPlay)

part1 fileLines =
    let plays = map getPlaysPart1 fileLines
    in sum $ map scoreLine plays

part2 fileLines =
    let plays = map getPlaysPart2 fileLines
     in sum $ map scoreLine plays

getFileLines filename = do
    content <- readFile filename
    return $ lines content

main' filename = do
   linesOfFile <- getFileLines filename

   print $ "Part 1: " ++ show (part1 linesOfFile)
   print $ "Part 2: " ++ show (part2 linesOfFile)

main = do
   args <- getArgs
   main' (args !! 0)
