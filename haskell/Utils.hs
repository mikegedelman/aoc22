module Utils (getFileLines, aocMain) where

import System.Environment

getFileLines filename = do
    content <- readFile filename
    return $ lines content

aocMain part1 part2  filename = do
   linesOfFile <- getFileLines filename

   putStrLn $ "Part 1: " ++ show (part1 linesOfFile)
   putStrLn $ "Part 2: " ++ show (part2 linesOfFile)
