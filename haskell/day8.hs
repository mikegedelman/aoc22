import Data.List
import Utils

get4Directions ds (i, j) =
    let row = ds !! i
        col = (transpose ds) !! j
        val = row !! j
        (top, bottom) = fmap (drop 1) $ splitAt i col
        (left, right) = fmap (drop 1) $ splitAt j row
    in (val, [(reverse top), bottom, (reverse left), right])

getInteriorCoords lines = [(i, j) | i <- [1..(length lines) - 2],
                                    j <- [1..(length $ head lines) - 2]]

getInteriorDirections lines = map (get4Directions lines) (getInteriorCoords lines)

containsGtEq x = (foldl (||) False . map (>= x))

isVisible (val, directions) = foldl (||) False $ map (not . containsGtEq val) directions

countTrue = foldl (\i v -> if v then i + 1 else i) 0

part1 lines = (countTrue . (map isVisible) . getInteriorDirections) lines + ((2 * (length lines)) - 4) + (2 * (length (head lines)))

-- Part2
visibilityScore val direction =
    let (lowerThan, rest) = span (< val) direction
    in (length lowerThan) + (if length rest == 0 then 0 else 1)

totalVisibilityScore (val, directions) = foldr1 (*) $ map (visibilityScore val) directions

part2 = maximum . (map totalVisibilityScore) . getInteriorDirections

main = aocMain part1 part2 "../inputs/day8.txt"
