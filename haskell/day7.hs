import Data.List
import Utils

example = [ "$ cd /", "$ ls", "dir a", "14848514 b.txt", "8504156 c.dat", "dir d", "$ cd a", "$ ls", "dir e", "29116 f", "2557 g", "62596 h.lst", "$ cd e", "$ ls", "584 i", "$ cd ..", "$ cd ..", "$ cd d", "$ ls", "4060174 j", "8033020 d.log", "5626152 d.ext", "7214296 k" ]

-- Parse the raw input into a sequence of ChangeDirectory and LsOutput
data DirectoryRaw = ChangeDirectory String | LsOutput [String] deriving (Show, Eq)

isPromptLine s = (take 1 s) == "$"

toDirectoryRaw accum [] = reverse accum
toDirectoryRaw accum (cur:rest)
    | cur == "$ ls" = let (lsOutputLines, rest') = span (not . isPromptLine) rest
                      in  toDirectoryRaw ((LsOutput lsOutputLines):accum) rest'
    | (take 4 cur) == "$ cd" = toDirectoryRaw ((ChangeDirectory (drop 5 cur)):accum) rest
    | otherwise = error $ "unexpected line: " ++ cur

-- Transform the sequence of cd and ls into a tree
data DirectoryTree = File (String, Int) | Directory String [DirectoryTree] deriving (Show, Eq)

dirDataLineToNode l =
    let [left, right] = words l
    in if left == "dir"
        then Directory right []
        else File (right, read left :: Int)

dirNameEq testName (Directory name _ ) = name == testName
dirNameEq _ _= False

-- Build a tree representing the file system.
insertDirectoryTree [] node (Directory dirname children) = Directory dirname (node:children)
insertDirectoryTree (cur:rest) node (Directory name children) =
    let ([targetDir], otherDirs) = partition (dirNameEq cur) children
    in Directory name ((insertDirectoryTree rest node targetDir):otherDirs)

insertListDirectoryTree path [] tree = tree
insertListDirectoryTree path (node:rest) tree =
    let tree' = insertDirectoryTree path node tree
    in insertListDirectoryTree path rest tree'

toDirectoryTree _ accumTree [] = accumTree

toDirectoryTree dirStack accumTree ((LsOutput dirData):rest) =
    let children = map dirDataLineToNode dirData
        accumTree' = insertListDirectoryTree (reverse dirStack) children accumTree
    in toDirectoryTree dirStack accumTree' rest

toDirectoryTree dirStack accumTree ((ChangeDirectory cdDir):rest) =
    let newDirStack = if cdDir == ".."
                      then (drop 1 dirStack)
                      else (cdDir:dirStack)
    in toDirectoryTree newDirStack accumTree rest

-- Build a tree that represents the size of each directory, removing the individual file nodes.
data SummaryTree = DirectorySum String Int [SummaryTree] deriving (Show, Eq)

isDirectory (Directory _ _) = True
isDirectory _ = False

getFileSize (File (_, size)) = size

getDirectorySumSize (DirectorySum _ size _) = size

collapseChildren (Directory dirname children) =
    let (subdirs, files) = partition isDirectory children
        fileSizes = sum $ map getFileSize files
        summarySubdirs = map collapseChildren subdirs
        subdirsSize = sum $ map getDirectorySumSize summarySubdirs
        totalDirSize = fileSizes + subdirsSize
    in DirectorySum dirname totalDirSize summarySubdirs

-- Output a list of all bytes from the summary tree
dirSumToList (DirectorySum _ bytes children) = (bytes:(concat $ map dirSumToList children))

getDirectorySummary lines =
    let dirRaw = toDirectoryRaw [] (drop 1 lines)
        dir = toDirectoryTree [] (Directory "/" []) dirRaw
    in collapseChildren dir

part1 = sum . filter (<= 100000) . dirSumToList . getDirectorySummary

part2 lines = (minimum . (filter (>= minimumDeleteSize)) . dirSumToList) dirSum
    where
        dirSum = getDirectorySummary lines
        usedSpace = getDirectorySumSize dirSum
        freeSpace = 70000000 - usedSpace
        minimumDeleteSize = 30000000 - freeSpace

main = aocMain part1 part2 "../inputs/day7.txt"
