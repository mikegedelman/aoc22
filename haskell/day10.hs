import Control.Monad.Trans.State (State, evalState, get, put)
import Data.List
import Data.List.Split
import Utils

step (cycles, x) = (cycles + 1, x)
noop = get >>= \s -> (put $ step s) >> return [step s]

addx x' = get >>= \s -> (put $ (step2 . add) s) >> return [step s, step2 s]
    where step2 = (step . step)
          add (cycles, x) = (cycles, x + x')

exec ["noop"] = noop
exec ["addx", s] = addx (read s ::Int)

allCycles = concat . flip evalState (0, 1) . mapM (exec . words)

part1 = foldl (\accum (_, b) -> accum + b) 0 . filter (\(n, _) -> (n + 20) `mod` 40 == 0) . allCycles

getPixel (n, x) = if ((n - 1) `mod` 40) `elem` [x - 1, x, x + 1] then '#' else '.'

part2 = concat . intersperse "\n" . chunksOf 40 . map getPixel . allCycles

main = do
   linesOfFile <- getFileLines "../inputs/day10.txt"
   putStrLn $ "Part 1: " ++ show (part1 linesOfFile)
   putStrLn "Part 2: "
   putStrLn $ part2 linesOfFile
