import Data.List
import qualified Data.Map as Map
import Utils

countDirBytes' _ accum [] = accum
countDirBytes' dirStack accum (cur:rest) = go (words cur)
    where
        go ("$" : "cd" : ".." : _) = countDirBytes' (drop 1 dirStack) accum rest
        go ("$" : "cd" : "/" : _) = countDirBytes' dirStack accum rest
        go ("$" : "cd" : dir : _) = countDirBytes' (dir:dirStack) accum rest
        go ("$" : "ls" : _) = countDirBytes' dirStack accum rest
        go ("dir" : _) = countDirBytes' dirStack accum rest
        go (bytes : _ ) =
            let counts = [(intercalate "/" d, read bytes :: Int) | d <- tails dirStack]
            in countDirBytes' dirStack (counts ++ accum) rest

countDirBytes = countDirBytes' [] []

dirMap = Map.fromListWith (+) . countDirBytes

part1 = sum . filter (<= 100000) . Map.elems . dirMap

part2 lines = (minimum . (filter (>= minimumDeleteSize)) . Map.elems) m
    where
        m = dirMap lines
        usedSpace = m Map.! ""
        freeSpace = 70000000 - usedSpace
        minimumDeleteSize = 30000000 - freeSpace

main = aocMain part1 part2 "../inputs/day7.txt"
