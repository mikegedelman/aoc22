
import System.Environment
import Data.Maybe
import Data.List

example = [ "1000", "2000", "3000", "", "4000", "", "5000", "6000", "", "7000", "8000", "9000", "", "10000" ]

toMaybeInt :: String -> Maybe Int
toMaybeInt "" = Nothing
toMaybeInt x = Just (read x :: Int)

groupCals :: [Maybe a] -> [[a]]
groupCals [] = []
groupCals xs = (map fromJust $ takeWhile isJust xs):(groupCals (drop 1 $ dropWhile isJust xs))

part1 :: [String] -> Int
part1 fileLines =
    let intList = map toMaybeInt fileLines
        groupedCals = groupCals intList
        summedCals = map sum groupedCals
        [result] = take 1 $ reverse $ sort $ summedCals
    in result

part2 :: [String] -> Int
part2 fileLines =
    let intList = map toMaybeInt fileLines
        groupedCals = groupCals intList
        summedCals = map sum groupedCals
        result = sum $ take 3 $ reverse $ sort $ summedCals
    in result

getFileLines filename = do
    content <- readFile filename
    return $ lines content

main = do
   args <- getArgs
   linesOfFile <- getFileLines (args !! 0)

   print $ "Part 1: " ++ show (part1 linesOfFile)
   print $ "Part 2: " ++ show (part2 linesOfFile)
