import System.Environment
import Data.List.Split
import Control.Monad (join)
import Control.Arrow ((***))

-- This is so we can map over a tuple, to map converstion to an int over a tuple
-- idk why this works lol
-- https://stackoverflow.com/questions/9722689/haskell-how-to-map-a-tuple
mapTuple = join (***)

example = [ "2-4,6-8", "2-3,4-5", "5-7,7-9", "2-8,3-7", "6-6,4-6", "2-6,4-8" ]

splitBy :: String -> String -> (String, String)
splitBy x s=
    let splitLn = splitOn x s
        (a:xs) = splitLn
        (b:_) = xs
    in (a, b)

rangeFullyContains :: (Int, Int) -> (Int, Int) -> Bool
rangeFullyContains r1 r2 =
    let (a1, b1) = r1
        (a2, b2) = r2
    in (a1 <= a2) && (b1 >= b2)

part1Parse l =
    let (range1Str, range2Str) = splitBy "," l
        range1 = mapTuple (\x -> read x ::Int) $ splitBy "-" range1Str
        range2 = mapTuple (\x -> read x ::Int) $ splitBy "-" range2Str
    in (range1, range2)

part1Line l =
    let (range1, range2) = part1Parse l
     in if (rangeFullyContains range1 range2 || rangeFullyContains range2 range1) then 1 else 0

part1 ls = sum $ map part1Line ls

-- Part 2
rangeOverlapsAtAll :: (Int, Int) -> (Int, Int) -> Bool
rangeOverlapsAtAll r1 r2 =
    let (a1, b1) = r1
        (a2, b2) = r2
    in ((a1 >= a2) && (a1 <= b2)) || ((b1 >= a2) && (b1 <= b2))

part2Line l =
    let (range1, range2) = part1Parse l
     in if (rangeOverlapsAtAll range1 range2 || rangeOverlapsAtAll range2 range1) then 1 else 0

part2 ls = sum $ map part2Line ls

-- Main
getFileLines filename = do
    content <- readFile filename
    return $ lines content

main' filename = do
   linesOfFile <- getFileLines filename

   print $ "Part 1: " ++ show (part1 linesOfFile)
   print $ "Part 2: " ++ show (part2 linesOfFile)

main = do
   args <- getArgs
   main' (args !! 0)
