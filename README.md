Advent of Code 2022

* [day 1](haskell/day1.hs) - originally done in [Rust](rust/src/bin/day1.rs), but switched to Haskell for fun.
* [day 2](haskell/day2.hs)
* [day 3](haskell/day3.hs)
* [day 4](haskell/day4.hs)
* [day 5](haskell/day5.hs)
* [day 6](haskell/day6.hs)
* [day 7](haskell/day7.hs)
    - [day 7 revision 1](haskell/day7rev1.hs) - after reading some other solutions, decided to write a much cleaner approach
* [day 8](haskell/day8.hs)
* [day 9](haskell/day9.hs)
* [day 10](haskell/day10.hs)
* [day 11](python/day11.py) - I didn't feel like implementing this in Haskell on a Sunday night.
* [day 12](python/day12.py) - these are getting harder, so I'm probably just switching to python going forward
* [day 13](python/day13.py)
* [day 14](python/day14.py)
* [day 15](cpp/day15.cu)
* [day 16](python/day16.py)
